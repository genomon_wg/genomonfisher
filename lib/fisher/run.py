#! /usr/bin/env python

import sys
import os
import argparse
import logging
import multiprocessing
import subprocess
import functools
import math
import time

import fisher


def partition_region(region, chunk_size, pileup_margin, output):
    ch, pos = region.split(':')
    start, end = map(int, pos.split('-'))
    num_max =  int(math.ceil(float(end - start + 1) / chunk_size)) - 1
    if num_max > 0:
        n = int(math.log10(num_max)) + 1
    else:
        n = 1
    #print "n =", n
    for i, x0 in enumerate(xrange(start, end+1, chunk_size)):
        x1 = min(x0 + chunk_size - 1, end)
        x0m = max(x0 - pileup_margin, start)
        x1m = min(x1 + pileup_margin, end)
        yield dict(region = '%s:%d-%d' % (ch, x0m, x1m),
                   out_file = output + ".%0*d" % (n + 1, i),
                   trim = (x0, x1))

def wrapper(func, args, new_args):
    args.update(new_args)
    t0 = time.time()
    func(**args)
    t = time.time() - t0
    print(new_args, "%f" % t)

def merge_files(output, header, itr):
    fout = open(output, 'w')
    if header:
        fout.write(header)
    for d in itr:
        fin = open(d['out_file'], 'r')
        for line in fin:
            fout.write(line)
        os.unlink(d['out_file'])


def run_compare(args):

    level = logging.getLevelName( args.log_level )

    if args.log_file:
        logging.basicConfig( filename   = args.log_file,
                             level      = level,
                             format     = '%(asctime)s %(message)s',
                             datefmt    ='%m/%d/%Y %I:%M:%S%p' )
    else:
        logging.basicConfig( level      = level,
                             format     = '%(asctime)s %(message)s',
                             datefmt    ='%m/%d/%Y %I:%M:%S%p' )
    #
    # Main function
    #
    if args.num_threads == 1:
        fisher.Pileup_and_count( 
             in_bam1 = args.bam1,
             in_bam2 = args.bam2,
             out_file = args.output,
             ref_fa = args.ref_fa,
             baseq_thres = args.base_quality,
             mismatch_rate_disease = args.min_allele_freq,
             mismatch_rate_normal = args.max_allele_freq,
             post_10_q = None,
             fisher_threshold = args.fisher_value,
             min_depth = args.min_depth,
             max_depth = args.max_depth,
             print_header = args.print_header,
             mapq_thres = args.mapping_quality,
             min_variant_read = args.min_variant_read,
             samtools = args.samtools_path,
             region = args.region
           )
    else:
        pool = multiprocessing.Pool(args.num_threads)

        run_Pileup_and_count = functools.partial(wrapper, fisher.Pileup_and_count,
                                                 dict(in_bam1 = args.bam1,
                                                      in_bam2 = args.bam2,
                                                     #out_file = args.output,
                                                      ref_fa = args.ref_fa,
                                                      baseq_thres = args.base_quality,
                                                      mismatch_rate_disease = args.min_allele_freq,
                                                      mismatch_rate_normal = args.max_allele_freq,
                                                      post_10_q = None,
                                                      fisher_threshold = args.fisher_value,
                                                      min_depth = args.min_depth,
                                                      max_depth = args.max_depth,
                                                     #print_header = args.print_header,
                                                      print_header = None,
                                                      mapq_thres = args.mapping_quality,
                                                      min_variant_read = args.min_variant_read,
                                                      samtools = args.samtools_path,
                                                     #region = args.region
                                                      ))

        pool.map(run_Pileup_and_count,
                 partition_region(args.region, args.chunk_size, args.pileup_margin, args.output))
        
        pool.close()

        if args.print_header:
            header_str = "#chr\tstart\tend\tref\tobs\tA,C,G,T\tA,C,G,T\tdis_mis\tdis_s_ratio\tctrl_mis\tctrl_s_ratio\tfisher\n"
        else:
            header_str = None
        merge_files(args.output, header_str,
                    partition_region(args.region, args.chunk_size, args.pileup_margin, args.output))


def run_single(args):

    level = logging.getLevelName( args.log_level )

    if args.log_file:
        logging.basicConfig( filename   = args.log_file,
                             level      = level,
                             format     = '%(asctime)s %(message)s',
                             datefmt    ='%m/%d/%Y %I:%M:%S%p' )
    else:
        logging.basicConfig( level      = level,
                             format     = '%(asctime)s %(message)s',
                             datefmt    ='%m/%d/%Y %I:%M:%S%p' )
    #
    # Main function
    #
    fisher.Pileup_and_count( 
            in_bam1 = args.bam1,
            in_bam2 = None,
            out_file = args.output,
            ref_fa = args.ref_fa,
            baseq_thres = args.base_quality,
            mismatch_rate_disease = args.min_allele_freq,
            mismatch_rate_normal = None,
            post_10_q = args.post_10_q,
            fisher_threshold = None,
            min_depth = args.min_depth,
            print_header = args.print_header,
            mapq_thres = args.mapping_quality,
            min_variant_read = args.min_variant_read,
            samtools = args.samtools_path,
            region = args.region
          )

